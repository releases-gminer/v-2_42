GMiner


GMiner was created by a Russian group of specialists in the field of high performance computing and cryptography.
The first version of GMiner was released on September 21, 2018 and was received quite warmly among users.
Thanks to its unique developments and stability, in just six months, the miner became a favorite on the Equihash algorithms.
The miner is focused on NVIDIA and AMD platforms and supports most popular algorithms such as: Ethash, ProgPoW, KAWPOW, Equihash, CuckooCycle.
GMiner maintains a leading position in the mining of such coins as Beam, Grin, Cortex, Bitcoin Gold.
In 2020, the miner added support for Ethash, ProgPoW and KAWPOW algorithms with high performance relative to competitors.
The development team never stops at what has been achieved and achieves the maximum performance of the algorithms with the minimum power consumption, it is these qualities that distinguish GMiner from the competitors and win the hearts of users.


Miner Features:

- commission is charged continuously, and not in intervals (as in most miners), which has a positive effect on the user's profitability on PPLNS pools
- verifying generated DAG, warning when GPU overclocking is very high for Ethash, Etcash, KAWPOW and ProgPoW algorithms, helps to overclock GPU without errors
- verifying Shares on processor, warning when GPU overclocking is very high for Ethash, Etcash, KAWPOW and ProgPoW algorithms, helps to overclock GPU without errors
- DAG caching if the GPU has enough memory, DAG files are not recomputed when switching to another algorithm when mining Ethash + Zilliqa or Nicehash, which has a positive effect on user profitability
- temperature control and stop the GPU in case of overheating
- watchdog - process-observer of state of main systems of the miner, which will restart the miner in case of crash or freeze
- mechanism to restore lost connection with pool
- supporting failover pools, the miner uses failover pools until the connection with the main pool is restored
- support secure connections, server certificate check (optional)
- informative and readable tabular statistics output to console
- display of detailed information on each device (temperature, power consumption, cooler load, memory frequency, processor frequency, energy efficiency)
- parallel output of information to console and to file on disk
- built-in statistics server - remote monitoring of the miner in browser
